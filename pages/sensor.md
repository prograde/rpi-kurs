---
title: Läs av omvärlden med en sensor
slug: sensor
date: 2021-09-19
---

Tack vare GPIO kan man koppla in olika sensorer för att läsa av omvärlden på olika sätt - man kan ge sin Raspberry Pi sinnen!

I det här exemplet, som är lite mer avancerat, kommer vi att använda programmeringsspråket [Python](https://python.org). Vi kommer också att behöva koppla lite mer.

Distansmätaren [HC-SR04](https://www.m.nu/sensorer-matinstrument/hc-sr04-ultrasonic-sensor) mäter avstånd genom att skicka ut en ultraljudssignal. Ljudet studsar mot väggen eller mot ett föremål framför distansmätaren, och genom att mäta hur lång tid det tar för ljudet att komma tillbaka, kan man räkna ut avståndet.

Distansmätaren har fyra pinnar: plus och jord (GND) samt en som man triggar ljudsignalen med (trigger) och en som man lyssnar efter ekot (echo).

## Python-koden

```
import RPi.GPIO as GPIO
import time
 
GPIO.setmode(GPIO.BCM)
triggerPin = 20 # väljer du en annan pin, så ändra här
echoPin = 16 # väljer du en annan pin, så ändra här
ljudhastighet = 34300 # för att beräkna avståndet, behöver vi veta ljudhastigheten

GPIO.setup(triggerPin, GPIO.OUT)
GPIO.setup(echoPin, GPIO.IN)
 
def distance():
    # gör ett kort "tut" med triggern
    GPIO.output(triggerPin, True)
    time.sleep(0.00001)
    GPIO.output(triggerPin, False)
 
    starttid = time.time()
    stopptid = time.time()
 
    # sätt starttiden, vänta tills echo är av
    while GPIO.input(echoPin) == 0:
        starttid = time.time()
 
    # stoppa timern när echoPinnen slår på
    while GPIO.input(echoPin) == 1:
        stopptid = time.time()
 
    tid = stopptid - starttid;
    distans = ljudhastighet * tid / 2
 
    return distans
 
if __name__ == '__main__':
    try:
        while True:
            print(f"Distans: {distance()}")
            time.sleep(1)
 
    # avbryt med Ctrl-C
    except KeyboardInterrupt:
        print("Stoppad")
        GPIO.cleanup()
```

## Kopplingen

![Koppling för distansmätare](../images/koppla/distansmatare.png "Koppla in distansmätaren")

I ovanståden kopplingsschema visas en Arduino, som är en typ av mikrokontroller. Hur man kopplar in distansmätaren är dock väldigt lika på en Raspberry Pi: Koppla den röda sladden till 5 V, den svarta till jord (GND) och den gröna (triggern) och den blå (echo) till två valfria GPIO-pinnar - se bara till att det stämmer överens med koden!)

Det övre motståndet i skissen ska vara 330 ohm, och det nedre 470 ohm.
