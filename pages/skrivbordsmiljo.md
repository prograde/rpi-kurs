---
title: Skrivbordsmiljön
slug: skrivbordsmiljo
---

## Installera skrivbordsmiljö

Tills nu ska vi använt Raspberry Pi OS i textläge: vi har givit våra kommandon till systemet som textkommandon, men vi har också använt ett menydrivet program, _raspi-config_, som är lite mer att betrakta som ett _grafiskt gränssnitt_ till systemet. 

Nu ska vi installera en fullfjädrad skrivbordsmiljö, med fönster, ikoner, menyer och muspekare, som ni kanske är vana vid från Windows eller Mac OS. Egentligen består en skrivbordmiljö av flera olika lager, eller komponenter som är ansvariga för olika saker.

* En _display server_ pratar med systemets kärna om vad som ska ritas på skärmen. Den vi ska använda oss av heter _XOrg_, och är den vanligaste inom Linux-världen. En annan vanlig heter _Wayland_. 
* En _display manager_ som är ett program som startar display-servern, visar inloggningsrutan och sedan startar igång skrivbordsmiljön. Den vi ska använda heter `lightdm`. En annan vanlig heter _GDM3_ och ytterligare en heter _slim_. 
* En _desktop environment_, skrivbordsmiljön, som är det _grafiska gränssnittet_ med fönster, ikoner, menyer och muspekare. Vi ska installera en som heter _pixel_, men här finns en rad andra att välja mellan: _Gnome, KDE, Mate, Cinnamon, XFCE med flera_

## Pakethanteraren _pi-package_

När vi har installerat skrivbordsmiljön kommer vi fortfarande att kunna använda _apt_ för att installera nya programpaket. Men det finns grafiska pakethanterare också. Den som hör till Raspberry Pi heter _pi-package_.

## Installation

Innan vi installerar behöver vi uppdatera listan av tillgängliga paket. Skriv:

`sudo apt update`

Därefter kan vi installera det som behövs. Vill man installera flera programpaket på samma gång, är det bara att skriva paketnamnen efter varandra:

`sudo apt install xserver-xorg lightdm raspberrypi-ui-mods pi-package`

apt går igenom vilka övriga programpaket som behövs för att installera de vi angav, och som vi ser är det är väldigt många beroenden. Det kommer att ta en stund att installera. Lika bra att ta en paus ...

När allt är installerat, starta om din Raspberry Pi med `sudo shutdown -r now`, och datorn kommer att starta om i skrivbordsläge!

## Inställningar

Några övningar:

- Om klockan går fel, prova att hitta inställningarna för att ställa in rätt tidszon
- Prova att ändra bakgrunden
- Försök att lägga till ytterligare en användare, och logga in som den användaren

Nu är det dags att gå vidare och [koppla in omvärlden](../koppla-in-omvarlden/index.html)!
