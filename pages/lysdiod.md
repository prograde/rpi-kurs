---
title: Tänd en lampa
slug: lysdiod
date: 2020-10-04
---

Nu ska vi testa på att koppla in några enheter till Raspberry Pi.

## Koppla in en lysdiod

Nu ska vi testa om det går att tända en lysdiod, genom att programmera vår Rasberry Pi. **Se först till att Rasberry Pi är avstängd.**

Först ska vi koppla in lysdioden. Men allra först ska vi koppla in en *extension board*:

![Extension Board](../images/koppla/diod0.jpg "Extension Board")

Här är Raspberry Pi inkopplad till en extension board som sitter på en kopplingsplatta (*breadboard*). Här är en närbild på vår lysdiod som vi vill få att lysa på något sätt.


![Lysdiod på kopplingsplatta](../images/koppla/diod5.jpg "Lysdiod på kopplingsplatta")

För att få den att lysa behöver vi leda ström genom den. På bilden här under har vi markerat alla pins med vita, **och 2 röda** punkter. Kan du se de röda punkterna?

![Markerade pins på kopplingsplatta](../images/koppla/diod5-1_LI.jpg "Breadboard")

Man kan programmera Rasberry Pi att slå på och av strömmen på varje sån här "Pin". Vi kommer använda oss av två pins:
- GND, som betyder ground eller jord på svenska.
- GPIO18.

På bilden har vi kopplat in jord (GND) från Raspberry Pi till kopplingsplattans kord (- i blått).

![Kopplingsplatta med gröna markeringar](../images/koppla/diod5_LI.jpg "Breadboard")

Strömmen ska passera igenom lysdioden som de gröna linjerna markerar. Vi kopplar in GND och GPIO18, så här:

![Kablar kopplade från GPIO 18 och GND](../images/koppla/diod3.jpg "Breadboard")

Om vi slår på strömmen på GPIO18, och leder den (via röd kabel) genom lysdioden, tillbaka till GND (svart kabel).

## Programmera

Vi behöver sätta upp vår utvecklingsmiljö, genom att installera Scratch 3. Vanligtvis kör man Scratch via webbläsaren, men för att kunna använda GPIO-kopplingarna, behöver vi installera Scratch som desktop-applikation. Antingen via 

![Add / Remove Software](../images/koppla/add-remove-software.png "Add / Remove Software")

eller genom att köra den här kommandona:
`sudo apt-get update`
`sudo apt-get install scratch3`

Nu ska vi sätta igång och programmera. Öppna Scratch 3.

![Open Scratch 3](../images/koppla/open-scratch.png "Open Scratch 3")

När du har öppnat Scratch, lägg till tillägget "GPIO", så här.

![Lägg till GPIO-tillägget](../images/koppla/add-GPIO.png "Lägg till tillägget GPIO")

Gå tillbaka och nu kan vi äntligen starta att programmera vår lysdiod. Lägg till det här programmet i Scratch.

![Program](../images/koppla/program.jpg "Program in Scratch to light the diod")

Nu är det dags att prova att få indata från omvärlden till Scratch. Fortsätt nu till sidan [Tryck på en knapp](../knapp/index.html)!
