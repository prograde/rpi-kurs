---
title: Hämta Raspberry Pi OS
slug: hamta
date: 2020-09-22
---
# Raspberry Pi OS 

Om du har använt Windows eller MacOS, har troligtvis operativsystemet redan varit installerat när du fick datorn. Nu ska vi göra det själva från grunden.

## Hämta Raspberry Pi OS Lite

Det första vi ska göra är att hämta det operativsystem som vi ska använda. Vi laddar ned det från Raspberry Pi:s [nedladdningssida](https://www.raspberrypi.org/downloads/raspberry-pi-os/). Här finns tre varianter:

1. Raspberry Pi OS with desktop and recommended software (med skrivbordsmiljö och rekommenderade program)
2. Raspberry Pi OS with desktop (med skrivbordsmiljö, men minimalt med program)
3. Raspberry Pi OS Lite (utan skrivbordsmiljö, bara textbaserad, främst avsedd för servrar)

Vi ska använda oss av den tredje versionen. Den innehåller ingen skrivbordsmiljö, för den ska vi installera själva senare. Operativsystemet kommer i form av en image-fil. Det är en avbildning av ett helt filsystem, som innehåller operativsystemets alla kataloger och filer. Den ska vi skriva till ett sd-kort, ett minneskort. Minneskortet rekommenderas vara minst 16 GB stort.

För att det ska gå snabbare att hämta image-filen, är den komprimeradoch har zip som filändelse.

1. Klicka på "Download ZIP". Hämtningen startar.
2. Packa upp zip-filen. Högerklicka på den och välj Extract eller Extrahera
3. Nu ska vi skriva imagefilen till sd-kortet. Det går att göra med programmet [Raspberry Pi Imager](https://www.raspberrypi.org/downloads/) som finns att hämta på Raspberry Pi:s webbplats, men det går även att göra med andra program för att skriva image-filer.
4. När image-filen har skrivits färdigt, kan du mata ut kortet, och sätta in det i din Raspberry Pi.

Nu är det dags för nästa steg: att [koppla ihop](../koppla-ihop/index.html) din Raspberry Pi. Det är snabbt gjort. 
