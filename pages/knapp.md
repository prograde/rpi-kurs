---
title: Tryck på en knapp
slug: knapp
date: 2021-09-18
---

# Koppla in en knapp

Man kan också få Scratch att lyssna på när man trycker på en knapp. Koppla in en knapp så här:

![Knapp](../images/koppla/button.png "Sätt dit en knapp på kopplingsbrädan.")

Nu ska vi skriva ett program som får saker att hända på scenen i Scratch när man trycker på knappen:

## Lägg till ett tillägg i Scratch

Klicka på Tilläggsmmenyn längst ner till vänster.

![Tillägg](../images/koppla/extension%20for%20button.png "Sätt dit en knapp på kopplingsbrädan.")

![Tillägg för knapp](../images/koppla/extension%20for%20button%202.png "Sätt dit en knapp på kopplingsbrädan.")

Så... nu ska vi sätta igång och programmera knappen. Lägg till det här programmet i Scratch.

![Skript för att lyssna på knapptryckning](../images/koppla/program%20for%20listen%20to%20a%20button.png "Lyssna på en knapptryckning.")

## Utmaningar

1. Kan du göra ett skript som tänder lysdioden när du trycker på knappen, och släcker lysdioden om du trycker på knappen igen?
1. Koppla in två knappar till två olika GPIO-pins. Kan du göra så att man tänder lysdioden med den ena knappen och släcker den med den andra?
2. Koppla in två knappar till två olika GPIO-pins. Här finns ett enkelt spel som heter [Bugrace](https://scratch.mit.edu/projects/251300716/). Kan du modifiera spelet så att man styr det med knapparna istället för piltangenterna?
3. Kan du göra så att lysdioden tänds när man kommer i mål?

![Bugrace](../images/koppla/bugrace.png "Modifiera spelet Bugrace")

Om du vill prova något lite mer avancerat, gå till sidan [Läs av en sensor](../sensor/index.html)!
