---
title: Få igång din Raspberry Pi
slug: fa-igang
date: 2012-09-15 19:52:05 UTC
---

Dags att komma igång med din Raspberry Pi! I den här lektionen ska vi göra följande tre steg:

1. <b>Hämta operativsystemet</b> och lägga in det på minneskortet.
2. <p>Koppla ihop</b> din Raspberry Pi. Det är inte så mycket som ska kopplas ihop, men vi passar på att gå igenom alla delar.
3. Starta din Raspberry Pi och <b>installera operativsystemet</b>. Det är några inställningar som behöver göras.

Läs vidare under [Hämta systemet](../hamta/index.html)!
