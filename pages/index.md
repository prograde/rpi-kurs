---
title: Kom igång med Raspberry Pi
slug: index
date: 2020-09-20 19:52:05 UTC
---

I den här kursen lär vi oss att installera och börja använda Raspberry Pi. När vi har lärt oss grunderna kommer vi också att prova på att styra en lysdiod och läsa av en sensor med hjälp av Raspberry Pi.

# Vad är Raspberry Pi?

Raspberry Pi är en pytteliten dator som är mindre än en kortlek men som innehåller allt som behövs för att fungera som en vanlig dator. Den har skapats för att barn och unga ska kunna lära sig om datorer, men har också blivit mycket populär bland datorintresserade i allmänhet.

# Operativsystemet Linux

Ett operativsystem är det som får en dator att fungera och som gör att det går att köra andra program på datorn. Exempel på vanliga operativsystem som du nog har kommit i kontakt med är:
  
* Windows
* macOS
* iOS/ipadOS
* Android

Vi kommer att lära oss en del om operativsystemet GNU/Linux i den här kursen. Linux är kanske inte lika känt för vanliga användare men är ändå ett av världens mest använda operativsystem. Det speciella med Linux är att det är fri programvara: vem som helst får 

* gratis hämta och använda det, 
* studera dess källkod och se hur det fungerar, 
* anpassa det för sina behov, och
* dela med sig av anpassningarna
 
# Koppla in omvärlden!

Raspberry Pi har in- och utgångar, GPIO, där det går att koppla in saker till exempel lysdioder, tryckknappar, sensorer och motorer. Dessa kan man sedan styra med sin Raspberry Pi. Vi ska lära oss hur man kopplar in en lysdiod och en tryckknapp och programmerar lysdioden till att tändas och släckas när vi trycker på knappen.

# Kom igång

Kursen är indelad i fyra delar som visas i meny på denna kurssida:
1. Få igång
2. Lär känna
3. Skrivbordsmiljön
4. Koppla
Börja med att gå till [Få igång](../fa-igang/index.html)!
 
