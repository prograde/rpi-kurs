---
title: Installera Raspberry Pi OS
slug: installera
date: 2020-09-22
---

När du har satt igång din Raspberry Pi för första gången, startas installationen av Raspberry Pi OS automatiskt. Du ser en massa textrader som rullar förbi på skärmen. Det tar några minuter för systemet att installeras, och när den är klar uppmanar den dig att logga in. Systemet är nu färdigt att användas, men det är några inställningar som vi vill göra först.

1. *Ställa in rätt tangentbordslayout:* Rasberry Pi är förinställt på att använda ett tangentbord med brittisk layout. Vi vill nu ställa in så att tangentbordslayouten matchar det tangentbord vi har (svenskt eller amerikanskt)
2. *Koppla upp oss mot nätverket via WiFi*
3. *Uppgradera systemet till den senaste versionen:* Även fast vi hämtade den nyaste versionen av operativsystemet, finns det förmodligen några delar av systemet som det har kommit nya versioner av.
4. *Skapa ett eget användarkonto:* Det finns ett förvalt användarkonto, men du ska få skapa ditt eget konto.

## Logga in

Det du ser nu är _inloggningsprompten_: `raspberrypi login:`. För att kunna göra något alls med systemet måste man logga in. Det förvalda användarnamnet är _pi_ och lösenordet _raspberry_.

## raspi-config

Punkt ett och två ovan, kan man göra med hjälp av programmet _raspi-config_. För att starta det, skriv `raspi-config` vid kommandoprompten. Då startar Raspberry Pi:s konfigurationsprogram.

### Tangentbordslayout

Olika språk har olika tangentbordslayout. För att datorn ska veta vilket tangentbord just du använder, måste du tala om det för datorn. det kan du göra med raspi-config.

### Koppa upp Raspberry Pi

För att koppla upp Raspberry Pi via Wifi, måste du veta Wifi-namnet (SSID) och lösenord. Med raspi-config ställer du in det i din Raspberry Pi.

## Pakethanteraren apt

För att uppgradera systemet så att alla ingående program blir av senaste version, använder vi pakethanteraren _apt_ ("program" kallas ofta "paket" i Linux-system) som hämtar de senaste paketen från nätet. Vi uppgraderar i två steg: 

1. I systemet finns en lista över tillgängliga paket. Steg ett är att uppdatera själva listan. Vid prompten, skriv `sudo apt update`. Du kommer nu att få skriva in lösenordet igen: _raspberry_. _sudo_ behöver man skriva före när man ska använda vissa program eller kommandon. Vi återkommer till det senare. Med _apt_ anropar vi programmet apt, och till apt skickar instruktionen _update_ som betyder uppdatera listan över paket.

2. När apt har uppdaterat sin lista, ska vi uppgradera de paket som det finns nya versioner av. Vid prompten skriv `sudo apt upgrade`.

Det kan ta ett tag att uppgradera, men när det är klart så har du ett nytt, fräscht system.

## Skapa en användare

Skapa en användare:

`sudo adduser ditt_namn`

Eftersom det krävs administratörsrättigheter för att lägga till en användare, måste du skriva `sudo` framför. `adduser` är ett program som lägger till användaren. Programmet kommer att fråga efter ett användarnamn och ett lösenord. Det frågare också efter telefonnummer, kontorsrum med mera, men det kan vi ignorera genom att bara trycka Retur på de frågorna.

Eftersom du med din nya användare också kommer att vilja utföra administratörssysslor, behöver du göra din användare till administratör:

`sudo usermod -aG sudo ditt_namn`

Kommandot `usermod` betyder *modifiera användare*. Växeln `-aG` står för *append group*, det vill säga, lägg in användaren i en grupp, och gruppen som vi vill lägga till användaren i heter *sudo*. Sist skriver man användarnamnet. 

Nu kan du logga ut, och sen logga in igen med den användare som du har skapat. För att se om du har lyckats med att få administratörsrättigheter, prova att skriva ett kommando med `sudo` framför, exempelvis `sudo apt update`.

Gå nu vidare till nästa kapitel: [Lär känna din Raspberry Pi](../lar-kanna.html).
